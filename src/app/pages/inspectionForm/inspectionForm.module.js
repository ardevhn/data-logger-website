/**
 * Created by camilo on 4/23/17.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.inspectionForm', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('inspectionForm', {
                url: '/inspectionForm',
                templateUrl: 'app/pages/inspectionForm/inspectionForm.html',
                title: 'Inspection Form',
                sidebarMeta: {
                    order: 800,
                    icon: 'fa fa-file-text-o'
                }
            });
    }

})();