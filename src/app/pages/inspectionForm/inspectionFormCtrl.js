/**
 * Created by camilo on 4/23/17.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.inspectionForm')
        .controller('inspectionFormCtrl', inspectionFormCtrl);

    /** @ngInject */
    function inspectionFormCtrl($scope, $http, $timeout, $element) {
        var now = new Date();
        var hr = now.getHours();
        var min = now.getMinutes();
        if (min < 10) {
            min = "0" + min;
        }
        var ampm = hr < 12 ? "AM" : "PM";
        hr = hr > 12 ? hr - 12 : hr;
        $scope.now = hr + ' : ' + min + ' ' + ampm;


        $scope.tractorSelection = [];
        $scope.tractorOptions = [
            {name: 'Air Compressor', column:1},
            {name: 'Air Lines', column:1},
            {name: 'Battery', column:1},
            {name: 'Belts and Hoses', column:1},
            {name: 'Body', column:1},
            {name: 'Brake Accesories', column:1},
            {name: 'Brakes, Parking', column:1},
            {name: 'Brakes, Service', column:1},
            {name: 'Clutch', column:1},
            {name: 'Coupling Devices', column:1},
            {name: 'Defroster/Heater', column:1},
            {name: 'Drive Line', column:1},
            {name: 'Engine', column:1},
            {name: 'Exhaust', column:1},
            {name: 'Fifth Wheel', column:1},
            {name: 'Fluid Levels', column:1},
            {name: 'Frame and Assembly', column:1},
            {name: 'Front Axle', column:2},
            {name: 'Fuel Tanks', column:2},
            {name: 'Horn', column:2},
            {name: 'Lights', column:2, extra: [
                'Head/Sto',
                'Tail/Das',
                'Turn Indicator',
                'Clearance/Marke'
            ]
            },
            {name: 'Mirrors', column:2},
            {name: 'Muffler', column:2},
            {name: 'Oil Pressure', column:2},
            {name: 'Radiator', column:2},
            {name: 'Rear End', column:2},
            {name: 'Reflectors', column:2},
            {name: 'Safety Equipment', column:3, extra:[
                'Fire Extinguishe',
                'Flags/Flares/Fusee',
                'Reflective Triangle',
                'Spare Bulbs and Fuse',
                'Spare Seal Bea'
            ]
            },
            {name: 'Starter', column:3},
            {name: 'Steering', column:3},
            {name: 'Suspension System', column:3},
            {name: 'Tire Chains', column:3},
            {name: 'Tires', column:3},
            {name: 'Transmission', column:3},
            {name: 'Trip Recorder', column:3},
            {name: 'Wheels and Rims', column:3},
            {name: 'Windows', column:3},
            {name: 'Windshield Wipers', column:3},
            {name: 'Other', column:3}
        ];


        $scope.isChecked = function(option){
            var isSelected = false;
            $scope.tractorSelection.forEach(function(item){
                if( item.name === option){
                    isSelected = true;
                }
            });
            return isSelected;
        };

        $scope.toggleSelection = function(option) {
            var idx = $scope.tractorSelection.indexOf(option);
            // Is currently selected
            if (idx > -1) {
                $scope.tractorSelection.splice(idx, 1);
            }
            // Is newly selected
            else {
                $scope.tractorSelection.push(option);
            }
        };



        $scope.trailerSelection = [];
        $scope.trailerOptions = [
            { name: 'Brake Connections', column:1},
            { name: 'Brakes', column:1},
            { name: 'Coupling Devices', column:1},
            { name: 'Coubpling (King) Pin', column:1},
            { name: 'Doors', column:1},
            { name: 'Hitch', column:2},
            { name: 'Landing Gear', column:2},
            { name: 'Lights - All', column:2},
            { name: 'Reflectos/Reflective Tape', column:2},
            { name: 'Roof', column:2},
            { name: 'Suspension System', column:3},
            { name: 'Trapaulin', column:3},
            { name: 'Tires', column:3},
            { name: 'Wheels and Rims', column:3},
            { name: 'Other', column:3}
        ];


        $scope.isChecked2 = function(option){
            var isSelected = false;
            $scope.trailerSelection.forEach(function(item){
                if( item.name === option){
                    isSelected = true;
                }
            });
            return isSelected;
        };


        $scope.toggleSelection2 = function(option) {
            var idx = $scope.trailerSelection.indexOf(option);
            // Is currently selected
            if (idx > -1) {
                $scope.trailerSelection.splice(idx, 1);
            }
            // Is newly selected
            else {
                $scope.trailerSelection.push(option);
            }
        };
    }
})();