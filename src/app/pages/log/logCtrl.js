/**
 * Created by camilo on 4/22/17.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.log')
        .controller('logCtrl', logCtrl);

    /** @ngInject */
    function logCtrl($scope, $http, $timeout, $element) {
        $scope.exampleData = [
            {
                "key": "Series 1",
                "values": [[0, 0], [1, 0], [2, 0], [3, 0], [4, 0], [5, 0],
                    [6, 0], [7, 0], [8, 0], [9, 0], [10, 0], [11, 0],
                    [12, 0], [13, 0], [14, 0], [15, 0], [16, 0], [17, 0],
                    [18, 0], [19, 0], [20, 0], [21, 0], [22, 0], [23, 0]]
            }];

        $scope.yAxisFormatFunction = function () {
            return function(d){
                return 'hola';
            }
        };

        $scope.currentDates = [ "04-11-2017", "04-12-2017", "04-14-2017"];

        $scope.currentDate = 0;

        $scope.showPrevious = function() {
            if ($scope.currentDate === 0){
                return;
            }
            $scope.currentDate -= 1;

            getReport()
        };

        $scope.showNext = function() {
            if ($scope.currentDate === 2){
                return;
            }
            $scope.currentDate += 1;

            getReport()
        };

        var getReport = function() {
            $http({
                method: 'GET',
                url: 'https://private-d586c-datalogger2.apiary-mock.com/api/rawdata/report/' + $scope.currentDates[$scope.currentDate]
            }).then(function successCallback(response) {
                $scope.reportResults = response.data;
            }, function errorCallback(response) {
                $scope.reportResults = [];
                alert("Error fetching report.")
            });
        };

        var now = new Date();
        var hr = now.getHours();
        var min = now.getMinutes();
        if (min < 10) {
            min = "0" + min;
        }
        var ampm = hr < 12 ? "AM" : "PM";
        hr = hr > 12 ? hr - 12 : hr;
        $scope.now = hr + ' : ' + min + ' ' + ampm;

        getReport();
    }
})();