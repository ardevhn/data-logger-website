/**
 * Created by camilo on 4/22/17.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.log', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('log', {
                url: '/log',
                templateUrl: 'app/pages/log/log.html',
                title: 'Log',
                sidebarMeta: {
                    order: 800,
                    icon: 'fa fa-book'
                }
            });
    }

})();