/**
 * Created by camilo on 4/23/17.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.uploadCsvFile', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('uploadCsvFile', {
                url: '/uploadCsvFile',
                templateUrl: 'app/pages/uploadCsvFile/uploadCsvFile.html',
                title: 'Upload File',
                sidebarMeta: {
                    order: 800,
                    icon: 'fa fa-upload'
                }
            });
    }

})();