/**
 * Created by camilo on 4/23/17.
 */
(function () {
    'use strict';

    angular.module('BlurAdmin.pages.uploadCsvFile')
        .controller('uploadCsvFileCtrl', uploadCsvFileCtrl);

    uploadCsvFileCtrl.$inject = ['$scope', 'fileReader', '$http', '$timeout', '$element', 'toastr', 'Upload'];

    /** @ngInject */
    function uploadCsvFileCtrl($scope, fileReader, $http, $timeout, $element, toastr, Upload) {
        $scope.progress = 0;

        $scope.selectFile = function () {
            var fileInput = document.getElementById('uploadFile');
            fileInput.click();

            if(fileInput.files.length != 0 ) {
                var firstFile = fileInput.files[0];
                $scope.fileNameModel = firstFile.name;
            }
        };

        $scope.uploadFile = function(file){
            try{
                if (file) {
                    file.upload = Upload.upload({
                        url: 'https://data-logger-api-dev.herokuapp.com/reports/upload-log',
                        headers: {
                            'Content-Type': 'multipart/form-data'
                        },
                        data: { file: file, name: file.name, description: file.description}
                    });

                    file.upload.then(function (response) {
                        $scope.progress = 100;
                        toastr.success('The File has been successfully uploaded.', 'Success');
                        $timeout(function () {
                            $scope.progress = 0;
                        }, 1000);

                    }, function (response) {

                        if(response.status == 500){
                            toastr.error(response.data, 'Error');
                            $scope.progress = 0;
                        }

                    }, function (evt) {
                        $scope.progress = Math.min(50, parseInt(50.0 * evt.loaded / evt.total))/100;
                    });
                }
            }catch(e){
                toastr.error('An error occurred while uploading the file.', 'Error');
            }
        }

        $scope.getFile = function () {
        };
    }
})();